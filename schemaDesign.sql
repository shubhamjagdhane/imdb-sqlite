
CREATE TABLE [imdbcast]
(
pid TEXT references imdbperson,
mid TEXT references imdbmovie,
role TEXT,
primary key(pid,mid)
);

CREATE TABLE [imdbdirector_genre]
(
did TEXT references imdbdirectors,
genre TEXT,
prob TEXT,
primary key(did,genre)
);

CREATE TABLE [imdbdirectors]
(
did TEXT primary key,
fname TEXT,
lname TEXT
);

CREATE TABLE [imdbgenre]
(
mid TEXT references imdbmovie,
genre TEXT,
primary key(mid,genre)
);

CREATE TABLE [imdbmovie_directors]
(
did TEXT references imdbdirectors,
mid TEXT refrences imdbmovie,
primary key(did,mid)
);


CREATE TABLE [imdbmovie]
(
mid TEXT primary key,
name TEXT,
year TEXT,
rank TEXT

);


CREATE TABLE [imdbperson]
(
pid TEXT primary key,
fname TEXT,
lname TEXT,
gender TEXT
);

