Q:1) 
select moviename from (select distinct(name) as moviename , (select count(p.pid) from imdbperson p , imdbcast c , imdbdirectors d , imdbmovie_directors md where imdbmovie.mid=c.mid and p.pid=c.pid and imdbmovie.mid=md.mid and d.did=md.did and gender='M') totcountM , (select count(p.pid) from imdbperson p , imdbcast c , imdbdirectors d , imdbmovie_directors md where imdbmovie.mid=c.mid and p.pid=c.pid and imdbmovie.mid=md.mid and d.did=md.did and gender='F') totcountF from imdbmovie where totcountM>2 and totcountF>2);
/*To check whether the number of male actors and female actors both are really greater than 3*/
select distinct(name) , (select count(p.pid) from imdbperson p , imdbcast c , imdbdirectors d , imdbmovie_directors md where imdbmovie.mid=c.mid and p.pid=c.pid and imdbmovie.mid=md.mid and d.did=md.did and gender='M') totcountM , (select count(p.pid) from imdbperson p , imdbcast c , imdbdirectors d , imdbmovie_directors md where imdbmovie.mid=c.mid and p.pid=c.pid and imdbmovie.mid=md.mid and d.did=md.did and gender='F') totcountF from imdbmovie where totcountM>2 and totcountF>2;

Q:2)
select name,d.fname,d.lname,c.fname,c.lname from imdbmovie m, imdbdirectors d, imdbmovie_directors md , imdbperson c where c.fname=d.fname and c.lname=d.lname and d.did=md.did and m.mid=md.mid limit 10;

Q:3)
select m.mid , m.name from (select pid,did from imdbperson p , imdbdirectors d where p.fname=d.fname and p.lname=d.lname) actdir , imdbmovie m , imdbcast c , imdbmovie_directors md where actdir.pid=c.pid and actdir.did=md.did and m.mid=c.mid and m.mid=md.mid limit 10;

Q:4)
select demo.pid,c,fname,lname from (select pid,count(mid) c from imdbcast group by pid limit 100) demo, imdbperson p where c>=(select sum(a)/count(pid) avgrage from  (select pid,count(mid) a from imdbcast group by pid)) and p.pid=demo.pid;

/*to verify above query*/
select c.pid , count(mid) from (select demo.pid,c,fname,lname from (select pid,count(mid) c from imdbcast group by pid limit 100) demo, imdbperson p where c>=(select sum(a)/count(pid) avgrage from  (select pid,count(mid) a from imdbcast group by pid)) and p.pid=demo.pid) main ,imdbcast c where  main.pid=c.pid group by c.pid;

Q:5)
select name , totper from (select name,count(c.pid) totper from imdbcast c , imdbmovie m where c.mid=m.mid group by c.mid) totmovie where totmovie.totper>=10 limit 10;

Q:6)
select mid , max(m) ming from (select mid , count(genre) m from imdbgenre group by mid) group by mid having ming=(select max(m) from (select mid , count(genre) m from imdbgenre group by mid));

Q:7)
select mid , min(m) ming from (select mid , count(genre) m from imdbgenre group by mid) group by mid having ming=(select min(m) from (select mid , count(genre) m from imdbgenre group by mid)) limit 20;

Q:8)
select p.pid,genre,count(g.mid) from imdbgenre g , imdbperson p , imdbcast c where p.pid=c.pid and c.mid=g.mid group by p.pid,g.mid limit 10;

Q:9)
select d.did,genre,count(g.mid) totMovieCount from imdbgenre g , imdbdirectors d, imdbmovie_directors md where d.did=md.did and md.mid=g.mid group by d.did,g.mid,genre limit 10;

Q:10)
select p.pid , fname , lname,(max(year)-min(year)) diff from imdbmovie m , imdbcast c , imdbperson p where m.mid=c.mid and p.pid=c.pid group by p.pid having diff<50 order by diff desc limit 30;

Q:11)
select year , count(mid) from imdbmovie group by year limit 20;

Q:12)
select year , count(m.mid) totMovieCount, fname , lname from imdbmovie m , imdbmovie_directors md , imdbdirectors d where m.mid=md.mid and d.did = md.did group by year having totMovieCount>0 limit 30;

Q:13)
select name from (select c.mid as mid from (select c.pid as pid ,count(distinct(genre)) cg from imdbcast c, imdbgenre g where g.mid=c.mid group by c.pid,c.mid having cg>5) demo , imdbcast c where demo.pid=c.pid limit 10) xyz , imdbmovie m where xyz.mid=m.mid;

